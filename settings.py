

KILL_ROUGE_PROCESS = True

# Uploading interval in seconds.
UPLOAD_INTERVAL = 24 * 3600         # 24 hours


try:
    from local_settings import *
except ImportError:
    pass
