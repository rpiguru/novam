#!/usr/bin/env bash

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo apt update

sudo apt install -y python3-dev build-essential cmake
sudo apt install -y python3-pip

sudo pip3 install -U pip
sudo pip3 install -r requirements.txt

echo "Disabling the booting logo..."
echo "disable_splash=1" | sudo tee -a /boot/config.txt
#sudo sed -i -- "s/$/ logo.nologo quiet loglevel=3 vt.global_cursor_default=0 systemd.show_status=0 plymouth.ignore-serial-consoles plymouth.enable=0/" /boot/cmdline.txt
#sudo sed -i -- "s/console=tty1/console=tty3/" /boot/cmdline.txt

# Disable the blinking cursor
#sudo sed -i -- "s/^exit 0/TERM=linux setterm -foreground black >\/dev\/tty0\\nexit 0/g" /etc/rc.local
#sudo sed -i -- "s/^exit 0/TERM=linux setterm -clear all >\/dev\/tty0\\nexit 0/g" /etc/rc.local

# Disable some services to reduce booting time
#sudo systemctl disable hciuart
#sudo mkdir /etc/systemd/system/networking.service.d
#sudo touch /etc/systemd/system/networking.service.d/reduce-timeout.conf
#echo "[Service]" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
#echo "TimeoutStartSec=1" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
#sudo rm /etc/systemd/system/dhcpcd.service.d/wait.conf

# Enable auto start
sudo apt install screen

sudo sed -i -- "s/^exit 0/screen -mS n -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S n -X stuff \"cd \/home\/pi\/novam\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S n -X stuff \"python3 main.py\\\\r\"\\nexit 0/g" /etc/rc.local
