import os
import signal
import subprocess
import logging.config

import psutil

try:
    import pwd
except ImportError:
    print('Are you running Windows OS?')


_cur_dir = os.path.dirname(os.path.realpath(__file__))

logging.config.fileConfig(os.path.join(_cur_dir, 'logging.ini'))
logger = logging.getLogger('Novam')


def get_username():
    return pwd.getpwuid(os.getuid())[0]


def is_rpi():
    try:
        return 'arm' in os.uname()[4]
    except AttributeError:
        return False


def get_kernel_info():
    pipe = os.popen("uname -a")
    data = pipe.read().strip()
    pipe.close()
    return data


def get_serial():
    """
    Get serial number of the device
    :return:
    """
    if is_rpi():
        cpuserial = "0000000000000000"
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26].lstrip('0')
        f.close()
        return cpuserial
    else:
        return '12345678'


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    try:
        if len(os.popen("ps -aef | grep -i '%s' "
                        "| grep -v 'grep' | awk '{ print $3 }'" % proc_name).read().strip().splitlines()) > 0:
            return True
    except Exception as e:
        print('Failed to get status of the process({}) - {}'.format(proc_name, e))
    return False


def check_running_pid(pid):
    """
    Check if a given PID does exist or not.
    :param pid:
    :return:
    """
    if psutil.pid_exists(pid):
        p = psutil.Process(pid)
        return p.name()


def kill_process_by_name(proc_name):
    """
    Kill process by its name
    :param proc_name:
    :return:
    """
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.decode().splitlines():
        if proc_name in line:
            pid = int(line.split(None, 1)[0])
            print('Found PID({}) of `{}`, killing...'.format(pid, proc_name))
            os.kill(pid, signal.SIGKILL)


def kill_process_by_pid(pid):
    try:
        p = psutil.Process(pid)
        p.kill()
    except Exception as e:
        logger.error('Failed to kill process({}) - {}'.format(pid, e))


def get_cpu_usage():
    return psutil.cpu_percent()


def get_cpu_temperature():
    if is_rpi():
        temp_file = '/sys/class/thermal/thermal_zone0/temp'
        return int(open(temp_file, 'r').read()) / 1000.
    else:
        return 36.5


def get_gpu_temperature():
    if is_rpi():
        temp = os.popen("vcgencmd measure_temp").readline()
        return float(temp.split('=')[1].split('\'C')[0])
    else:
        return 36.5


def get_mem():
    """
    Get memory usage
    """
    pipe = os.popen("free -tm | grep 'Total' | awk '{print $2,$3,$4}'")
    data = pipe.read().strip().split()
    pipe.close()

    all_mem = int(data[0])
    used_mem = int(data[1])
    free_mem = int(data[2])

    percent = round(used_mem * 100. / all_mem, 1)

    return {'used': used_mem, 'total': all_mem, 'free': free_mem, 'percent': percent}


def get_up_time():
    pipe = os.popen("uptime")
    data = pipe.read().strip().split(',')[0]
    pipe.close()
    return ' '.join(data.split()[-2:])


def get_process_detail():
    """
    Get UID, PPID, C, STIME, TTY, TIME, CMD of ALL running processes
    :return:
    """
    pipe = os.popen('ps -ef')
    data = pipe.read().strip().splitlines()
    pipe.close()
    p_data = {}
    for i in range(1, len(data)):
        buf = str(data[i]).split()
        p_data[int(buf[1])] = dict(
            UID=buf[0],
            PID=int(buf[1]),
            PPID=int(buf[2]),
            C=int(buf[3]),
            STIME=buf[4],
            TTY=buf[5],
            TIME=buf[6],
            CMD=" ".join(buf[7:])
        )
    return p_data


def read_file_content(file_name):
    return [l.strip() for l in open(file_name, 'r').readlines() if not l.startswith('#') and l.strip()]


if __name__ == '__main__':

    tmp = []
    for _p, d in get_process_detail().items():
        tmp.append(d['CMD'])

    for t in sorted(list(set(tmp))):
        print(t)
