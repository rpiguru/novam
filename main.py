import hashlib
import os
import threading
import time
import pprint

from settings import UPLOAD_INTERVAL, KILL_ROUGE_PROCESS
from utils import logger, kill_process_by_pid, get_process_detail, read_file_content, get_username, get_kernel_info, \
    get_cpu_usage, get_cpu_temperature, get_mem, get_up_time, get_serial


_cur_dir = os.path.dirname(os.path.realpath(__file__))


class NovamSupervisor(threading.Thread):

    _stop = threading.Event()

    def __init__(self):
        super().__init__()
        self._stop.clear()

    def run(self):
        s_time = time.time()
        while not self._stop.isSet():
            allowed_list = read_file_content(os.path.join(_cur_dir, "processes.txt"))
            default_prefixes = read_file_content(os.path.join(_cur_dir, "process_prefix.txt"))
            for p, data in get_process_detail().items():
                cmd = data['CMD']
                if cmd not in allowed_list and not any(cmd.startswith(pre) for pre in default_prefixes):
                    logger.warning('A rouge process is detected. PID: {}, CMD: {}'.format(p, data['CMD']))
                    if KILL_ROUGE_PROCESS:
                        kill_process_by_pid(p)
            time.sleep(1)
            if time.time() - s_time > UPLOAD_INTERVAL:
                logger.info('### Uploading device status data...')
                kernel_info = get_kernel_info()
                data = {
                    'user': get_username(),
                    'kernel': kernel_info,
                    'kernel_hashed': hashlib.sha512(kernel_info.encode()).hexdigest(),
                    'serial_number': get_serial(),
                    'cpu': {
                        'used': get_cpu_usage(),
                        'temperature': get_cpu_temperature()
                    },
                    'memory': get_mem(),
                    'uptime': get_up_time(),

                }
                pprint.pprint(data)
                s_time = time.time()

    def stop(self):
        self._stop.set()


if __name__ == '__main__':

    logger.info("========== Starting Novam Supervisor ==========")
    ns = NovamSupervisor()
    ns.start()
