# Novam

Novam Project on Embedded Linux Devices

## Installation

- Clone this project into the home directory:
    
    
        cd ~
        git clone https://gitlab.com/rpiguru/novam.git

- Install
        
        
        bash setup.sh
        
- And reboot! :)

## How it works

- The main script is started automatically when RPi boots up.

- The main script continuously checks the content of `pid_list.txt` file in `/home/pi/novam` directory.
    Actually, it reads that file every second.

- If you add a process id into that file and save, the main script will read it and check all running processes to kill. 


So, please just open `pid_list.txt` and add pid to each line. (Create that file if not exists)

e.g.:

  * Launch dummy process
        
        
        python3 dummy_process.py &
        
     You will see the PID of this process.
     
  * Now, open `pid_list.txt` and add that pid to the last line. Our main script will kill it instantly.

## Monitoring

All logs are saved into `/var/log/novam.log`.

So just execute following command on the other terminal to monitor it:

    tail -f /var/log/novam.log
